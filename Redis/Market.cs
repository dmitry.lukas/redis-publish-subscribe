﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redis
{
    public class Market
    {
        public int FixtureId { get; set; }
        public int MarketTypeId { get; set; }
        public override string ToString()
        {
            return $"MARKET: [FixtureId={FixtureId}, MarketTypeId={MarketTypeId}]";
        }
    }
}
