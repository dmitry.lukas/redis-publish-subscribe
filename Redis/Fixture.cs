﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redis
{
    public class Fixture
    {
        public int FixtureId { get; set; }
        public DateTimeOffset StartTime { get; set; }
        public override string ToString()
        {
            return $"FIXTURE: [FixtureId={FixtureId}, StartTime={StartTime.ToString("G")}]";
        }
    }
}
