﻿using Newtonsoft.Json;
using StackExchange.Redis;

namespace Redis
{
    public class Client
    {
        string url = "localhost:6379";

        public void Publish<T>(T value)
        {
            var redis = ConnectionMultiplexer.Connect(url);
            var pub = redis.GetSubscriber();
            pub.Publish(typeof(T).Name, JsonConvert.SerializeObject(value));
        }

        //public void Subscribe<T>(Func<RedisValue, Task> handler)
        //{
        //    var redis = ConnectionMultiplexer.Connect(url);
        //    var sub = redis.GetSubscriber();
        //    sub.Subscribe(typeof(T).Name, (channel, message) => handler(message));
        //}

        public async Task SubscribeAsync<T>(Func<RedisValue, Task> handler)
        {
            var redis = await ConnectionMultiplexer.ConnectAsync(url);
            var sub = redis.GetSubscriber();
            await sub.SubscribeAsync(typeof(T).Name, (channel, message) => handler(message));
        }
    }
}