﻿// See https://aka.ms/new-console-template for more information
using Redis;
using StackExchange.Redis;

Console.WriteLine("Starting subscriber");

var redis = new Client();

await redis.SubscribeAsync<Fixture>(MessageAction);
await redis.SubscribeAsync<Market>(MessageAction);

Console.ReadLine();

 async Task MessageAction(RedisValue message)
{
    await Task.Delay(10);
    Console.WriteLine($"Received message {message}");
}

