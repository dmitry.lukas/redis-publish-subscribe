﻿// See https://aka.ms/new-console-template for more information
using Redis;

Console.WriteLine("Starting publisher");


//var redis = ConnectionMultiplexer.Connect("localhost:6379");
//var pub = redis.GetSubscriber();

var redis = new Client();

while (true)
{
    var fixture = new Fixture { FixtureId = 123, StartTime = DateTimeOffset.UtcNow};
    var market = new Market { FixtureId = fixture.FixtureId, MarketTypeId = 2};

    Console.WriteLine($"Publishing fixture {fixture}");
    redis.Publish(fixture);

    Thread.Sleep(2000);

    Console.WriteLine($"Publishing market {market}");
    redis.Publish(market);

    Thread.Sleep(3000);
}